---
title: CCOS Inaugura Novo Site
author: Deandreson Alves
---

O CCOS inaugura novo site para ser a nova forma de contato e de atualizações por
meio de postagens com o auxílio de ferramentas de livre acesso, fornecendo
informações para os usuários e tornar o Centro como fonte de referência.
